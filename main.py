from multiprocessing import Pool
from typing import Union
import re

import requests


def get_threads(board: str) -> list[list[str, int]]:
    print(f"getting threads from {board}...")
    response = requests.get(f"https://2ch.hk/{board}/catalog.json")
    try:
        response = response.json()
    except:
        print(f"error at {board}")
        return []
    thread_list = []
    if response['threads']:
        for thread in response['threads']:
            thread_list.append([board, thread['num']])
    return thread_list


def saving(board: list[str, int]):
    print(f"getting {board[1]} from {board[0]}...")
    response = requests.get(f"https://2ch.hk/{board[0]}/res/{board[1]}.json")
    try:
        response = response.json()
    except:
        print(f"error at {board}")
        return []
    texts = []

    for post in response['threads'][0]['posts']:
        post = post['comment'].split("</a><br>")

        if len(post) == 0:
            post = post[0]
        elif len(post) == 2:
            post = post[1]
        else:
            post = post[len(post)-1]

        if post != "":
            # remove weird characters from post
            post = post.replace('<span class="spoiler">', "").replace("</span>", '')
            post = post.replace('<span class="unkfunc">', "")
            post = post.replace('<span class="s">', "")
            post = post.replace("<span class="u"", "")
            post = post.replace('<br>', " ")
            post = post.replace("&lt;", "<")
            post = post.replace("&gt;", ">")
            post = post.replace("<strong>", "").replace("</strong>", '')
            post = post.replace("<em>", "").replace("</em>", "")
            post = post.replace("<p>", "").replace("</p>", '')
            post = post.replace("<b>", "").replace("</b>", '')
            post = post.replace("&#47;", " ")  # /, but we will use " "
            post = post.replace("&quot;", "")
            post = post.replace('<sup', "").replace("</sup", "")
            post = post.replace('<sub', "").replace("</sub", "")
            post = post.replace("\xa0", "")
            post = remove_tag_a(post)
            post = post.replace(">", "")
            texts.append(post)
    return texts


def remove_tag_a(text: str) -> str:
    tag = re.compile(r'<a.*?</a>')
    return tag.sub('', text)


def save_to_file(filename: str, data: list[str]) -> str:
    data = "\n".join(data)
    with open(f'storage/{filename}', 'w') as f:
        f.write(data)


def get_board_id() -> Union[str, list[int]]:
    # getting all boards from 2ch
    all_boards_raw = requests.get("https://2ch.hk/makaba/mobile.fcgi?task=get_boards").json()
    all_boards = []

    for boards in all_boards_raw:
        all_boards += list(map(lambda board: board['id'], all_boards_raw[boards]))
    all_boards.append("all")  # adding all, if we want to download all boards

    # input board id for downloading or input all for downloading whole boards
    not_selected_board = True
    board = ""
    while not_selected_board:
        print("INPUT ID:")
        board = input()
        if board in all_boards:
            not_selected_board = False

    all_boards.remove("all")  # remove all, bcz, it's not a board
    return board, all_boards


if __name__ == '__main__':
    board, all_boards = get_board_id()

    if board == 'all':
        threads = []
        for brd in all_boards:
            threads += get_threads(brd)
    else:
        threads = get_threads(board)

    print(f"Need to download: {len(threads)}")

    pool = Pool()
    texts = pool.map(saving, threads)
    pool.close()
    pool.join()

    texts = [item for sublist in texts for item in sublist]

    save_to_file(f'{board}.txt', texts)
