# 2ch-parser


## Info

This is a parser for 2ch.hk. It can download threads from boards.

<img src="Screenshot 2022-07-28 at 02.59.23.png" width="500"/>

Input ID is a name of board.
It will save all posts to storage/{input_id}.txt

It can download all boards from 2ch.hk, use "all" as input

# Installation

1. Clone: `git clone https://gitlab.com/SPUZ_/2ch-parser.git`
2. Create virtual environment `python3 -m venv venv`
3. Activate virtual environment `source venv/bin/activate`
4. Install requirements `pip3 install -r requirements.txt`
5. Start `python3 main.py`